const { groups } = require("./conspirators")


const plots = [
    {
        name: "Contrôle des pensées de la population à l'aide du réseau 5G",
        imageURL: "https://risibank.fr/cache/medias/0/14/1481/148123/full.png",
        groups: [ groups[0], groups[3] ],
    },

    {
        name: "Soumission des populations grâce à la menace du COVID-19",
        imageURL: "https://risibank.fr/cache/medias/0/24/2404/240462/full.png",
        groups: [ groups[0] ],
    },

    {
        name: "Avènement du nouvel ordre mondial",
        imageURL: "https://risibank.fr/cache/medias/0/3/310/31026/full.png",
        groups: [ groups[0], groups[2], groups[3] ],
    },

    {
        name: "Vassalisation de la France à la Chine",
        imageURL: "https://risibank.fr/cache/medias/0/16/1659/165905/full.png",
        groups: [ groups[1] ],
    },

    {
        name: "Avènement du bolchévisme en France par une révolution instrumentalisée",
        imageURL: "https://image.noelshack.com/fichiers/2016/38/1474755146-risitas734.png",
        groups: [ groups[1] ],
    },

    {
        name: "Soumission des êtres humains à l'être suprême reptilien",
        imageURL: "https://risibank.fr/cache/medias/0/25/2544/254465/full.png",
        groups: [ groups[2] ],
    },

    {
        name: "Surveillance généralisée du peuple grâce à l'IA",
        imageURL: "https://risibank.fr/cache/medias/0/6/653/65356/full.png",
        groups: [ groups[0], groups[1], groups[2], groups[3] ],
    },
]


exports.plots = plots
