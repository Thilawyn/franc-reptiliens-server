const { createRoom, formatRoomForNetwork, getRoom, deleteRoom, sendRoomUpdate, generateInitialRoomGameState, startRoom, roomNextTurn, sendRoomNextTurn } = require("./room")

const app = require("express")()
const http = require("http").Server(app)
const io = require("socket.io")(http)


io.on("connect", socket => {

    console.log("Client connected.")


    socket.on("room.create", data => {
        let { user } = data
        const room = createRoom(user)

        console.log(user.name + " created room n°" + room.id)
        user = {
            ...user,
            socket,
        }

        room.leader = user
        room.users.push(user)

        socket.emit("room.joined", { room: formatRoomForNetwork(room) })
    })

    socket.on("room.join", data => {
        const { user } = data
        const room = getRoom(data.roomID)

        if (!room || room.users.length >= 2 || room.inProgress) {
            socket.emit("room.unjoinable")
            return
        }

        console.log(user.name + " joined room n°" + room.id)
        room.users.push({
            ...user,
            socket,
        })

        sendRoomUpdate(room)
        socket.emit("room.joined", { room: formatRoomForNetwork(room) })
    })

    socket.on("room.leave", data => {
        const { user } = data
        const room = getRoom(data.roomID)

        if (!room || !room.users.find(el => el.id === user.id))
            return

        console.log(user.name + " left room n°" + room.id)
        room.users = room.users.filter(el => el.id !== user.id)

        if (room.users.length === 0 || room.leader.id === user.id) {
            room.users.forEach(roomUser => {
                roomUser.socket.emit("room.closed")
            })

            console.log("Room n°" + room.id + " is no more.")
            deleteRoom(room.id)
        }
        else
            sendRoomUpdate(room)
    })

    socket.on("room.start", data => {
        const room = getRoom(data.roomID)

        generateInitialRoomGameState(room)
        roomNextTurn(room)

        sendRoomUpdate(room)
        startRoom(room)
    })

    socket.on("room.investigatorPlayed", data => {
        const { roomID, choosedGroups, choosedPlots } = data
        const room = getRoom(roomID)

        if (choosedGroups) room.state = { ...room.state, choosedGroups: [...room.state.choosedGroups, ...choosedGroups] }
        if (choosedPlots)  room.state = { ...room.state, choosedPlots: [...room.state.choosedPlots, ...choosedPlots] }
        roomNextTurn(room)

        sendRoomUpdate(room)
        sendRoomNextTurn(room)
    })

})


http.listen(8033, () => {
    console.log("Listening on *:8033")
})
