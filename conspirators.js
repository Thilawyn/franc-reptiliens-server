const groups = [
    "Mondialistes",
    "Communistes",
    "Reptiliens",
    "Technocrates",
]


const conspirators = [
    {
        name: "Emmanuel Macron",
        imageURL: "https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/Emmanuel_Macron_%28cropped%29.jpg/220px-Emmanuel_Macron_%28cropped%29.jpg",
        groups: [ groups[0] ],
    },

    {
        name: "Nicolas Sarkozy",
        imageURL: "https://upload.wikimedia.org/wikipedia/commons/a/a6/Nicolas_Sarkozy_in_2010.jpg",
        groups: [ groups[0] ],
    },

    {
        name: "Angela Merkel",
        imageURL: "https://upload.wikimedia.org/wikipedia/commons/thumb/0/0f/Angela_Merkel_2019_cropped.jpg/220px-Angela_Merkel_2019_cropped.jpg",
        groups: [ groups[0] ],
    },

    {
        name: "Jacques Attali",
        imageURL: "https://cdn.radiofrance.fr/s3/cruiser-production/2021/03/13899459-d445-4f0c-9f20-25d045215bf1/1136_gettyimages-858436694.jpg",
        groups: [ groups[0] ],
    },

    {
        name: "Klaus Schwab",
        imageURL: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRHvC9FebPLHI7xYLgNp8KKzI_QNH_cR315Q1HT6IwwRdqjz5dtdxtUqXonMuhiRkfZmf8&usqp=CAU",
        groups: [ groups[0], groups[3] ],
    },

    {
        name: "Jean Castex",
        imageURL: "https://risibank.fr/cache/medias/0/22/2272/227256/full.png",
        groups: [ groups[0] ],
    },

    {
        name: "Larry Silverstein",
        imageURL: "https://image.noelshack.com/fichiers/2018/51/3/1545248326-larryreup.png",
        groups: [ groups[0] ],
    },

    {
        name: "Pfizer",
        imageURL: "https://pharmacie.ma/uploads/pfizer-nouveau-logo-500.jpg",
        groups: [ groups[0] ],
    },


    {
        name: "Parti Communiste",
        imageURL: "https://upload.wikimedia.org/wikipedia/commons/thumb/2/29/PRC_flags_in_Rome.JPG/220px-PRC_flags_in_Rome.JPG",
        groups: [ groups[1] ],
    },

    {
        name: "Kim Jong Un",
        imageURL: "https://upload.wikimedia.org/wikipedia/commons/thumb/6/6e/Kim_Jong-un_April_2019_%28cropped%29.jpg/220px-Kim_Jong-un_April_2019_%28cropped%29.jpg",
        groups: [ groups[1] ],
    },

    {
        name: "Staline Mort-vivant",
        imageURL: "https://www.akg-images.fr/Docs/AKG/Media/TR3_WATERMARKED/f/0/2/8/AKG4235927.jpg",
        groups: [ groups[1] ],
    },

    {
        name: "Le fantôme de Lénine",
        imageURL: "https://upload.wikimedia.org/wikipedia/commons/thumb/1/17/Vladimir_Lenin.jpg/220px-Vladimir_Lenin.jpg",
        groups: [ groups[1] ],
    },

    {
        name: "L'esprit de Karl Marx",
        imageURL: "https://upload.wikimedia.org/wikipedia/commons/f/fc/Karl_Marx.jpg",
        groups: [ groups[1] ],
    },

    {
        name: "Bernie Sanders",
        imageURL: "https://upload.wikimedia.org/wikipedia/commons/0/02/Bernie_Sanders_in_March_2020.jpg",
        groups: [ groups[1] ],
    },


    {
        name: "Confrérie des Francs Reptiliens",
        imageURL: "https://upload.wikimedia.org/wikipedia/commons/thumb/1/1e/Eye.jpg/220px-Eye.jpg",
        groups: [ groups[2] ],
    },

    {
        name: "Reine Elizabeth 2",
        imageURL: "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/Queen_Elizabeth_II_in_March_2015.jpg/220px-Queen_Elizabeth_II_in_March_2015.jpg",
        groups: [ groups[2] ],
    },

    {
        name: "George W. Bush",
        imageURL: "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d4/George-W-Bush.jpeg/220px-George-W-Bush.jpeg",
        groups: [ groups[0], groups[2] ],
    },

    {
        name: "George W. Bush",
        imageURL: "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d4/George-W-Bush.jpeg/220px-George-W-Bush.jpeg",
        groups: [ groups[0], groups[2] ],
    },

    {
        name: "Barack Obama",
        imageURL: "https://upload.wikimedia.org/wikipedia/commons/thumb/8/8d/President_Barack_Obama.jpg/220px-President_Barack_Obama.jpg",
        groups: [ groups[0], groups[2] ],
    },

    {
        name: "David Rockefeller",
        imageURL: "https://resize-europe1.lanmedia.fr/r/622,311,forcex,center-middle/img/var/europe1/storage/images/europe1/international/deces-du-milliardaire-philanthrope-americain-david-rockefeller-3007676/33502067-1-fre-FR/Deces-du-milliardaire-philanthrope-americain-David-Rockefeller.gif",
        groups: [ groups[0], groups[2] ],
    },


    {
        name: "Jeff Bezos",
        imageURL: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS_DksYEtY0ptOsACNXnkCXpyfxg8AXY24DHw&usqp=CAU",
        groups: [ groups[3] ],
    },

    {
        name: "Mark Zuckerberg",
        imageURL: "https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/Mark_Zuckerberg_F8_2019_Keynote_%2832830578717%29_%28cropped%29.jpg/260px-Mark_Zuckerberg_F8_2019_Keynote_%2832830578717%29_%28cropped%29.jpg",
        groups: [ groups[0], groups[2], groups[3] ],
    },

    {
        name: "Bill Gates",
        imageURL: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR9ZsmkNs0NLaetrx0jh1OrgeXtqA9AQaXTkA&usqp=CAU",
        groups: [ groups[0], groups[2], groups[3] ],
    },

    {
        name: "Tim Cook",
        imageURL: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS4b_Qy3qo0OR0-0P2lVBM4wQVeSYI9lnbhiw&usqp=CAU",
        groups: [ groups[3] ],
    },

    {
        name: "Elon Musk",
        imageURL: "https://cdn.futura-sciences.com/buildsv6/images/largeoriginal/d/9/a/d9a1058910_50163142_elon-musk1.jpg",
        groups: [ groups[3] ],
    },

    {
        name: "Sundar Pichai",
        imageURL: "https://www.webrankinfo.com/dossiers/wp-content/uploads/sundar-pichai-officiel.jpg",
        groups: [ groups[3] ],
    },

    {
        name: "Susan Wojcicki",
        imageURL: "https://upload.wikimedia.org/wikipedia/commons/thumb/0/0b/Susan_Wojcicki_%2829393944130%29_%28cropped%29.jpg/220px-Susan_Wojcicki_%2829393944130%29_%28cropped%29.jpg",
        groups: [ groups[3] ],
    },


    {
        name: "ET NON LES SHILLS",
        imageURL: "https://risibank.fr/cache/medias/0/1/187/18740/full.png",
        groups: [ ...groups ],
    },

    {
        name: "VITE MA DOSE AAAAAAH",
        imageURL: "https://risibank.fr/cache/medias/0/24/2407/240724/full.png",
        groups: [ ...groups ],
    },

    {
        name: "coucou",
        imageURL: "https://image.noelshack.com/fichiers/2018/25/2/1529422413-risitaszoom.png",
        groups: [ ...groups ],
    },

    {
        name: "MON PAS AGNIGNI",
        imageURL: "https://image.noelshack.com/fichiers/2021/29/1/1626731040-picsart-07-19-11-43-41.jpg",
        groups: [ ...groups ],
    },
]


exports.conspirators = conspirators
exports.groups = groups
