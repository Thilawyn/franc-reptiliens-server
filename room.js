const { groups, conspirators } = require("./conspirators")
const { plots } = require("./plots")

const roomsByID = {}
exports.roomsByID = roomsByID


const randomDigit = () =>
    (Math.floor(Math.random() * 9) + 1).toString()

const randomRoomID = () =>
    randomDigit() + randomDigit() + randomDigit() + randomDigit() + randomDigit() + randomDigit()


exports.getRoom = id =>
    roomsByID[ id ]

exports.createRoom = leaderUser => {
    let id
    while (!id || roomsByID[ id ])
        id = randomRoomID()

    const room = {
        id,
        leader: null,
        users: [],
        inProgress: false,
        state: {},
    }

    roomsByID[ id ] = room
    return room
}

exports.formatRoomForNetwork = room => ({
    ...room,

    leader: {
        ...room.leader,
        socket: undefined,
    },
    users:  room.users.map(user => ({
                ...user,
                socket: undefined,
            })),
    state: {
        ...room.state,

        investigatorUser: room.state.investigatorUser && {
            ...room.state.investigatorUser,
            socket: undefined,
        },
    },
})

exports.generateInitialRoomGameState = room => {
    room.state = {
        done: false,

        allGroups: [...groups],
        allConspirators: [...conspirators],
        allPlots: [...plots],

        jobSwitchCount: 0,

        choosedGroups: [],
        choosedPlots: [],
    }

    const conspiratorGroup = groups[ Math.floor(Math.random() * groups.length) ]
    const plotsOfGroup = this.getPlotsOfGroup(room, conspiratorGroup)
    const plot = plotsOfGroup[ Math.floor(Math.random() * plotsOfGroup.length) ]

    room.state = {
        ...room.state,

        conspiratorGroup,
        plot,
    }
}


const getConspiratorOfGroup = (room, turnAllConspirators) => {
    const persons = turnAllConspirators.filter(person => person.groups.includes(room.state.conspiratorGroup))
    if (persons.length === 0)
        return null

    return persons[ Math.floor(Math.random() * persons.length) ]
}

const getConspiratorNotOfGroup = (room, turnAllConspirators) => {
    const persons = turnAllConspirators.filter(person => !person.groups.includes(room.state.conspiratorGroup))
    if (persons.length === 0)
        return null

    return persons[ Math.floor(Math.random() * persons.length) ]
}

exports.removeConspirator = (room, conspirator) => {
    room.state.allConspirators = room.state.allConspirators.filter(person => person.name !== conspirator.name)
}


exports.getPlotsOfGroup = (room, group) =>
    room.state.allPlots.filter(plot => plot.groups.includes(group))


exports.getSortedChoosedGroups = room => {
    let groups = []
    room.state.choosedGroups.forEach(el => {
        if (!groups.includes(el))
            groups = [...groups, el]
    })

    return groups
        .map(group => ({
            name: group,
            count: room.state.choosedGroups
                .filter(el => el === group)
                .length,
        }))
        .sort((a, b) => a.count - b.count)
        .map(({ name }) => name)
}

exports.getSortedChoosedPlots = room => {
    let plots = []
    room.state.choosedPlots.forEach(el => {
        if (!plots.includes(el))
            plots = [...plots, el]
    })

    return plots
        .map(plot => ({
            value: plot,
            count: room.state.choosedPlots
                .filter(el => el === plot)
                .length,
        }))
        .sort((a, b) => a.count - b.count)
        .map(({ value }) => value)
}

exports.getMostPopularPlots = room => {
    let plots = []
    this.getSortedChoosedGroups(room).slice(0, 3)
        .forEach(group => {
            this.getPlotsOfGroup(room, group)
                .forEach(plot => {
                    if (!plots.includes(plot))
                        plots = [...plots, plot]
                })
        })

    return plots
}


exports.roomNextTurn = room => {
    const nextInvestigator = room.state.investigator ? 0 : 1
    const investigatorJob = (room.state.jobSwitchCount === 2) ? "plots" : "groups"
    if (room.state.jobSwitchCount === 2) room.state.jobSwitchCount = -1


    let done = false


    let turnAllConspirators = [...room.state.allConspirators]
    let turnConspirators = []

    if (investigatorJob === "groups") {
        for (let i = 0; i < 3; i++) {
            const person = getConspiratorOfGroup(room, turnAllConspirators)
            if (person) {
                turnAllConspirators = turnAllConspirators.filter(el => el.name !== person.name)
                turnConspirators.push(person)
            }
        }

        for (let i = 0; i < 2; i++) {
            const person = getConspiratorNotOfGroup(room, turnAllConspirators)
            if (person) {
                turnAllConspirators = turnAllConspirators.filter(el => el.name !== person.name)
                turnConspirators.push(person)
            }
        }

        turnConspirators = turnConspirators
            .map(value => ({ value, sort: Math.random() }))
            .sort((a, b) => a.sort - b.sort)
            .map(({ value }) => value)

        for (let i = 0; i < 3; i++)
            this.removeConspirator( room, turnConspirators[ Math.floor(Math.random() * turnConspirators.length) ] )

        if (turnConspirators.length < 4)
            done = true
    }


    let turnPlots = []

    if (investigatorJob === "plots")
        turnPlots = this.getMostPopularPlots(room)
            .map(value => ({ value, sort: Math.random() }))
            .sort((a, b) => a.sort - b.sort)
            .map(({ value }) => value)


    room.state = {
        ...room.state,

        done,

        investigator: nextInvestigator,
        investigatorUser: room.users[ nextInvestigator ],
        investigatorJob,

        jobSwitchCount: room.state.jobSwitchCount + 1,

        turnConspirators,
        turnPlots,
    }

    if (done) {
        room.inProgress = false

        room.state = {
            ...room.state,

            sortedChoosedGroups: this.getSortedChoosedGroups(room),
            sortedChoosedPlots: this.getSortedChoosedPlots(room),
        }
    }
}

exports.startRoom = room => {
    room.users.forEach(roomUser => {
        roomUser.socket.emit("room.started")
    })
}

exports.sendRoomNextTurn = room => {
    room.users.forEach(roomUser => {
        roomUser.socket.emit("room.nextTurn")
    })
}

exports.sendRoomUpdate = room => {
    room.users.forEach(roomUser => {
        roomUser.socket.emit("room.update", { room: this.formatRoomForNetwork(room) })
    })
}

exports.deleteRoom = id => {
    delete roomsByID[ id ]
}
